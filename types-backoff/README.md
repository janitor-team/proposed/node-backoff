# Installation
> `npm install --save @types/backoff`

# Summary
This package contains type definitions for backoff (https://github.com/MathieuTurcotte/node-backoff#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/backoff.

### Additional Details
 * Last updated: Wed, 07 Jul 2021 21:44:49 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [BendingBender](https://github.com/BendingBender).
